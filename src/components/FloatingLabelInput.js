import React, { Component } from 'react'
import { Animated, Text, TextInput, View } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome5'
import { colors, fonts } from '../services/Constants'
//Styles
import styles from './styles/FloatingLabelInputStyle'

export default class FloatingLabelInput extends Component {
  state = {
		isFocused : false,
		hideText	: this.props.secureTextEntry || false
	}

	UNSAFE_componentWillMount() {
		this._animatedIsFocused = new Animated.Value(this.props.value === '' ? 0 : 1)
	}

	handleFocus = () => this.setState({ isFocused: true })
	handleBlur = () => this.setState({ isFocused: false })

	componentDidUpdate() {
		Animated.timing(this._animatedIsFocused, {
			toValue	 : (this.state.isFocused || this.props.value !== '') ? 1 : 0,
			duration : 200
		}).start()
	}

	render() {
		const { isFocused, hideText } = this.state,
					{ label, ...props }		 = this.props,
					isPassword							= 'password' === this.props.textContentType,
					labelStyle							= {
						position: 'absolute',
						left: this._animatedIsFocused.interpolate({
							inputRange	: [0, 1],
							outputRange : [20, 10]
						}),
						top: this._animatedIsFocused.interpolate({
							inputRange	: [0, 1],
							outputRange : [16, 4]
						}),
						fontFamily: fonts.regular,
						fontSize: this._animatedIsFocused.interpolate({
							inputRange	: [0, 1],
							outputRange : [16, 12]
						}),
						color: this._animatedIsFocused.interpolate({
							inputRange	: [0, 1],
							outputRange : [colors.black, colors.primary]
						}),
					}
		return (
			<View style={[styles.container, props.first?styles.withoutMargin:{}, props.style]}>
				<Animated.Text style={labelStyle}>
					{label}
				</Animated.Text>
				<View style={[styles.subcontainer, isFocused?styles.selected:{}]}>
					<TextInput
						{...props}
						style={[this.props.multiline?styles.textarea:styles.input, isPassword?{ width: '90%' }:{}]}
						onFocus={this.handleFocus}
						onBlur={this.handleBlur}
						blurOnSubmit
						secureTextEntry={hideText}
						selectionColor={colors.light_blue}
						underlineColorAndroid="transparent"
					/>
					{
						isPassword?
						<Icon style={styles.icon} name={hideText?'eye-slash':'eye'} size={24} color={colors.primary} onPress={() => this.setState({ hideText: !hideText })} />:
						null
					}
				</View>
			</View>
		)
	}
}

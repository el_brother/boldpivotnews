import React from 'react'
import { Text } from 'react-native'
import Dialog, { DialogButton, DialogContent, DialogFooter, DialogTitle, SlideAnimation } from 'react-native-popup-dialog'
//Styles
import styles from './styles/PopupStyle'

export default Popup = (props) => {
	const renderFooter = () => {
		if(undefined != props.confirmText && undefined != props.cancelText) {
			return (
				<DialogFooter>
					<DialogButton
						bordered
						key="button-1"
						onPress={props.onConfirm}
						style={styles.confirmBtnBg}
						text={props.confirmText}
						textStyle={styles.confirmBtnTxt}
					/>
					<DialogButton
						bordered
						key="button-2"
						onPress={props.onClose}
						style={styles.cancelBtnBg}
						text={props.cancelText}
						textStyle={styles.cancelBtnTxt}
					/>
				</DialogFooter>
			)
		}
		else if(undefined != props.confirmText) {
			return (
				<DialogFooter>
					<DialogButton
						bordered
						key="button-1"
						onPress={props.onConfirm}
						style={styles.confirmBtnBg}
						text={props.confirmText}
						textStyle={styles.confirmBtnTxt}
					/>
				</DialogFooter>
			)
		}
		else if(undefined != props.cancelText) {
			return (
				<DialogFooter>
					<DialogButton
						bordered
						key="button-1"
						onPress={props.onClose}
						style={styles.cancelBtnBg}
						text={props.cancelText}
						textStyle={styles.cancelBtnTxt}
					/>
				</DialogFooter>
			)
		}
		else {
			return null
		}
	}
	return (
	    <Dialog
	    	actionsBordered
	    	dialogAnimation={new SlideAnimation({ slideFrom: 'top', toValue: 0, useNativeDriver: true })}
			dialogTitle={
				<DialogTitle
					align="center"
					hasTitleBar={false}
					style={styles.titlebg}
					textStyle={styles.titletxt}
					title={props.title}
				/>
			}
			footer={renderFooter()}
			overlayOpacity={0.5}
			rounded
			visible={props.show}
			width={0.9}
		>
			<DialogContent style={styles.content}>
				{
					'string' == typeof props.content?
					<Text style={styles.contentText}>{props.content}</Text>:
					props.content
				}
			</DialogContent>
		</Dialog>
	)
}

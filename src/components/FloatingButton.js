import React, { Component } from 'react'
import { Animated, Text, View } from 'react-native'
import ActionButton from 'react-native-action-button'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
//Services
import { colors } from '../services/Constants'
//Styles
import styles from './styles/FloatingButtonStyle'

export default class FloatingButton extends Component {
	constructor(props) {
		super(props)
		//Init state
		this.state = {
			fadeAnim : new Animated.Value(1),
			show     : true
		}
	}

	UNSAFE_componentWillReceiveProps(nextProps) {
		const { show } = nextProps
		if('boolean' == typeof show && this.state.show != show) {
			this.setState({ show }, () => true == show?this.fadeIn():this.fadeOut())
		}
	}

	fadeIn = () => {
		Animated.timing(
			this.state.fadeAnim,
			{
				toValue  : 1,
				duration : 500
			}
		).start()

	}

	fadeOut = () => {
		this.setState({ fadeAnim: new Animated.Value(0) })
		Animated.timing(
			this.state.fadeAnim,
			{
				toValue  : 0,
				duration : 500
			}
		).start()
	}

	render() {
		return (
			<ActionButton 
				size={this.state.show?56:0}
				buttonColor={this.props.color?this.props.color:colors.secondary}
				renderIcon={() => (
					<View style={styles.button}>
						<Icon name={this.props.icon?this.props.icon:'plus'} style={styles.icon} />
						{
							this.props.label?
							<Text style={styles.buttonText}>{this.props.label}</Text>:
							null
						}
					</View>
				)}
				onPress={this.props.onPress}
			/>
		)
	}
}
import React from 'react'
import { connect } from 'react-redux'
import { Text, View } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome5'
import { colors } from '../services/Constants'
//Styles
import styles from './styles/HeaderStyle'

class Header extends React.Component {
	render() {
		const { scene } = this.props
		return (
			<View style={styles.header}>
				{
					'Home' != scene.route.name?
					<Icon name="chevron-left" style={styles.left} onPress={ () => this.props.navigation.goBack() } />:
					<View style={styles.left}></View>
				}
				<Text style={styles.title}>{scene.descriptor.options.title}</Text>
				<View style={styles.right}></View>
			</View>
		)
	}
}

export default connect()(Header)

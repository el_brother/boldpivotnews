import React from 'react'
import { ActivityIndicator, View } from 'react-native'
import { colors } from '../services/Constants'
//Styles
import styles from './styles/LoadingStyle'

export default Loading = (props) => {
  	return (
    	<View style={[styles.container, props.style]}>
			<ActivityIndicator color={props.color || colors.primary} size={props.size || 'large'} />
		</View>
  	)
}

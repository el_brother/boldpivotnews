import React, { Component } from 'react'
import { TextInput, View } from 'react-native'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
//Components
import FloatingLabelInput from './FloatingLabelInput'
//Services
import { colors } from '../services/Constants'
//Styles
import styles from './styles/SearchInputStyle'

export default class SearchInput extends Component {

    constructor(props) {
        super(props)
        this.state = { to_search: '' }
    }

    clean = () => {
    	this.setState({ to_search: '' }, () => {
    		const { to_search } = this.state
    		const { onChangeText, onSubmitEditing } = this.props 
    		if(onChangeText) {
    			onChangeText(to_search)
    		} else if(onSubmitEditing) {
    			onSubmitEditing(to_search)
    		}
		})
    }

    onChangeText = (text) => {
    	const { onChangeText } = this.props 
    	this.setState({ to_search: text }, () => onChangeText?onChangeText(this.state.to_search):null)
    }

    onSubmitEditing = (e) => {
    	const { onSubmitEditing } = this.props
    	if(onSubmitEditing) {
    		onSubmitEditing(this.state.to_search)
   	 	}
    }

    render() {
    	const { to_search } = this.state,
    		  { multiline } = this.props
    	return (
			<View style={[styles.container, multiline?styles.multiline:{}]}>
				<TextInput
					blurOnSubmit={true}
					keyboardType="web-search"
					multiline={multiline}
					onChangeText={this.onChangeText}
					onSubmitEditing={this.onSubmitEditing}
					placeholder={this.props.label || 'search'}
					placeholderTextColor={colors.black}
					returnKeyType="search"
		            style={styles.input}
		            selectionColor={colors.light_gray}
		            underlineColorAndroid="transparent"
		            value={to_search}
	          	/>
		        {
		        	'' != to_search?
		        	<Icon name="close" style={styles.icon} onPress={() => this.clean()} />:
		        	<Icon name="magnify" style={styles.icon} />
		        }
		    </View>
	    )
	}
}
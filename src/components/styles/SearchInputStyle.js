import { StyleSheet } from 'react-native'
import { colors, fonts } from '../../services/Constants'

export default StyleSheet.create({
  container: {
    alignItems: 'center',
    backgroundColor: colors.white,
    borderRadius: 25,
    flexDirection: 'row',
    justifyContent: 'space-between',
    minHeight: 50,
    paddingHorizontal: 18,
    width: '100%'
  },
  icon: {
    color: colors.black,
    fontSize: 20
  },
  input: {
    backgroundColor: 'transparent',
    color: colors.black,
    fontFamily: fonts.regular,
    fontSize: 16,
    width: '80%'
  }
})
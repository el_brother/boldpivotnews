import { StyleSheet } from 'react-native'
import { colors } from '../../services/Constants'

export default StyleSheet.create({
    container: {
		alignItems: 'center',
        backgroundColor: colors.white,
        height: '100%',
		justifyContent: 'center',
	    width: '100%'
	}
})
import { StyleSheet } from 'react-native'
import { colors, fonts } from '../../services/Constants'

export default StyleSheet.create({
	container: {
		marginTop: 10,
		paddingTop: 4,
		width: '100%'
	},
	icon: {
		marginTop: 15
	},
	input: {
		backgroundColor: 'transparent',
		color: colors.black,
		fontFamily: fonts.regular,
		fontSize: 16,
		height: 55,
		paddingLeft: 20,
		paddingBottom: 0,
		width: '100%'
	},
	selected: {
		borderBottomColor: colors.primary
	},
	subcontainer: {
		borderBottomColor: colors.black,
		borderBottomWidth: 2,
		flexDirection: 'row',
		justifyContent: 'space-between',
		width: '100%'
	},
	textarea: {
		backgroundColor: 'transparent',
		color: colors.black,
		fontFamily: fonts.regular,
		fontSize: 16,
		height: 75,
		paddingLeft: 20,
		paddingBottom: 0,
		textAlignVertical: 'top',
		width: '100%'
	},
	withoutMargin: {
		marginTop: 0
	}
})


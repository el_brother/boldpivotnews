import { StyleSheet } from 'react-native'
import { colors, fonts } from '../../services/Constants'

export default StyleSheet.create({
	titlebg: {
		backgroundColor: 'transparent'
	},
	titletxt: {
		color: colors.primary,
		fontFamily: fonts.black,
		fontSize: 18,
		textAlign: 'center'
	},
	cancelBtnBg: {
		backgroundColor: colors.gray
	},
	cancelBtnTxt: {
		color: colors.white,
		fontFamily: fonts.regular,
		fontSize: 16
	},
	confirmBtnBg: {
		backgroundColor: colors.green
	},
	confirmBtnTxt: {
		color: colors.white,
		fontFamily: fonts.regular,
		fontSize: 16
	},
	content: {
		backgroundColor: 'transparent',
		maxHeight: 200,
		width: '100%'
	},
	contentText: {
		color: colors.black,
		fontFamily: fonts.bold,
		fontSize: 14
	}
})
import { StyleSheet } from 'react-native'
import { colors, fonts } from '../../services/Constants'

export default StyleSheet.create({
  author: {
    backgroundColor: 'transparent',
    color: colors.gray,
    fontFamily: fonts.regular,
    fontSize: 12,
    width: '90%'
  },
  authorContainer: {
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'flex-start'
  },
  body: {
    alignItems: 'center',
    backgroundColor: colors.secondary,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    width: '100%'
  },
  card: {
    marginTop: 10,
    width: '100%'
  },
  container: {
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'flex-start'
  },
  date: {
    backgroundColor: 'transparent',
    color: colors.primary,
    fontFamily: fonts.regular,
    fontSize: 12,
    marginBottom: 5,
    textAlign: 'right',
    width: '100%'
  },
  iconsFavorite: {
    color: colors.primary,
    fontFamily: fonts.regular,
    fontSize: 20,
    marginRight: 5
  },
  image: {
    borderRadius: 16,
    height: 64,
    width: 64
  },
  left: {
    alignItems: 'center',
    justifyContent: 'center',
    width: '24%'
  },
  ratingContainer: {
    alignItems: 'center',
    backgroundColor: 'transparent',
    justifyContent: 'center',
    marginTop: 5,
    width: '100%'
  },
  right: {
    padding: 5,
    width: '76%'
  },
  title: {
    backgroundColor: 'transparent',
    color: colors.white,
    fontFamily: fonts.bold,
    fontSize: 14,
    marginBottom: 5
  },
  withoutMargin: {
    marginTop: 0
  }
})
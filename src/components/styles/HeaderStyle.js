import { StyleSheet } from 'react-native'
import { colors, fonts } from '../../services/Constants'


export default StyleSheet.create({
	header: {
    	alignItems: 'center',
		backgroundColor : colors.primary,
		borderBottomWidth: 0,
    	flexDirection: 'row',
    	justifyContent: 'space-between',
    	padding: 10,
		shadowOpacity: 0,
		shadowOffset: {
  			height: 0,
		},
		shadowRadius: 0,
		elevation: 0,
		width: '100%'
  	},
  	title: {
	    color: colors.white,
	    fontFamily: fonts.regular,
	    fontSize: 16,
	    textAlign: 'center',
	    width: '80%'
  	},
  	left: {
  		color: colors.white,
	    fontFamily: fonts.regular,
	    fontSize: 25,
	    width: '10%'
  	},
 	 right: {
    	width: '10%'
	}
})
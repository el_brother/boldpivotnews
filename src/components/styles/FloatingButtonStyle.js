import { StyleSheet } from 'react-native'
import { colors, fonts } from '../../services/Constants'

export default StyleSheet.create({
	button: {
		alignItems: 'center',
		justifyContent: 'center'
	},
	buttonText: {
		backgroundColor: 'transparent',
		color: colors.white,
		fontFamily: fonts.regular,
		fontSize: 10,
		textAlign: 'center'
	},
	icon: {
		color: colors.white,
		fontFamily: fonts.regular,
		fontSize: 20
	}
})
import { StyleSheet } from 'react-native'
import { colors, fonts } from '../../services/Constants'

export default StyleSheet.create({
  iconsContainer: {
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'center'
  },
  icon: {
    color: colors.gray,
    fontFamily: fonts.regular,
    fontSize: 16
  },
  iconSelected: {
    color: colors.yellow,
    fontFamily: fonts.regular,
    fontSize: 16
  }
})
import React, { Component } from 'react'
import { TouchableOpacity } from 'react-native'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
//Styles
import styles from './styles/RatingStyle'

const icons = ['sad', 'neutral', 'happy', 'excited']

export default class Rating extends Component {

  	constructor(props) {
    	super(props)
  	}

  	onSelectIcon = (value) => {
  		const { item, onSelectRating } = this.props
  		if('function' == typeof onSelectRating) {
  			onSelectRating(item, value)
  		}
  	}

  	renderIcons = () => {
  		const { iconSize, onSelectRating, item } = this.props
  		return icons.map((icon, index) => {
  			if(onSelectRating) {
	  			return (
	  				<TouchableOpacity activeOpacity={1} key={index} onPress={() => this.onSelectIcon(index)}>
	  					<Icon name={`emoticon-${icon}`} style={[index == item.rating?styles.iconSelected:styles.icon, iconSize?{ fontSize: iconSize }:null]} />
					</TouchableOpacity>
				)
			} else {
				return (<Icon key={index} name={`emoticon-${icon}`} style={[index == item.rating?styles.iconSelected:styles.icon, iconSize?{ fontSize: iconSize }:null]} />)
			}
  		})
  	}

	render() {
		return (
			<TouchableOpacity activeOpacity={1} style={styles.iconsContainer} onPress={() => this.props.onPress()}>
				{this.renderIcons()}
		  	</TouchableOpacity>
		)
	}
}
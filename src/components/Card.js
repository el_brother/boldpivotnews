import React, { Component } from 'react'
import { Image, Text, TouchableOpacity, View } from 'react-native'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import { colors } from '../services/Constants'
//Components
import Rating from './Rating'
//Services
import { getAuthor } from '../services/Utils'
//Libs
import moment from 'moment'
//Styles
import styles from './styles/CardStyle'

export default class Card extends Component {

  	constructor(props) {
    	super(props)
    	this.state = { image: { uri: props.urlToImage } }
  	}

	onErrorImage = () => {
		this.setState({ image: require('../assets/images/default.png') })
	}

	renderContent = () => {
		const { image }  = this.state,
		      author     = getAuthor(this.props.author)
	  	return (
			<View style={styles.body}>
				<View style={styles.left}>
					<TouchableOpacity activeOpacity={1} onPress={() => this.props.onPress()}>
		  				<Image source={image} style={styles.image} onError={this.onErrorImage} resizeMethod="resize" />
	  				</TouchableOpacity>
	  				<View style={styles.ratingContainer}>
	  					<Rating item={this.props} onPress={this.props.onRatingPress} />
  					</View>
	  			</View>
	  			<View style={styles.right}>
		  			<Text style={styles.date}>{moment(this.props.publishedAt).fromNow()}</Text>
		  			<TouchableOpacity activeOpacity={1} onPress={() => this.props.onPress()}>
						<Text style={styles.title}>{this.props.title}</Text>
					</TouchableOpacity>
					<View style={styles.authorContainer}>
						<Text style={styles.author}>{author || 'Anonymous'}</Text>
						<Icon name={this.props.favorite?'heart':'heart-outline'} style={styles.iconsFavorite} onPress={this.props.onFavoritePress} />
					</View>
		  		</View>
			</View>
	  	)
	}

	render() {
	  const { index } = this.props,
			style	  = 0 == index?styles.withoutMargin:{}
		return (
			<View style={[styles.card, style]}>
				{this.renderContent()}
		  	</View>
		)
	}
}
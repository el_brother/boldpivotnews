import React from 'react'
import { connect } from 'react-redux'
import { Image, LayoutAnimation, ScrollView, Text, TouchableHighlight, View } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome5'
//Components
import Loading from '../../components/Loading'
import Rating from '../../components/Rating'
import FloatingButton from '../../components/FloatingButton'
//Redux
import { addNewsToFavorites, storeNews } from '../../actions/news'
import { closePopup, loadingPopup, showPopup } from '../../actions/popup'
//Services
import Api from '../../api/index'
import { colors } from '../../services/Constants'
import { getAuthor } from '../../services/Utils'
//Libs
import moment from 'moment'
//Styles
import styles from '../styles/ShowStyle'

class Show extends React.Component {
  	constructor(props) {
	    super(props)
	    const item = props.news.find(item => item.publishedAt == props.route.params.id)
	    //Init state
	    this.state = {
	    	image       : { uri: item.urlToImage },
	    	info        : item,
	    	loading     : false,
	    	show_button : true
	    }
	    //Local variables
	    this.scrollViewRef = null
  	}

	componentWillUnmount() {
		this.props.closePopup()
	}

	setScrollViewRef = (reference) => {
	    this.scrollViewRef = reference
	}

	rateNews = (item, rate) => {
  		const setRating = item => {
  			return {
				...item,
				...{ rating: rate  }
			}
  		}
  		const info = setRating(item)
  		this.setState({ info  }, () => {
  			this.props.storeNews(this.props.news.map(item2 => info.publishedAt == item2.publishedAt?setRating(item2):item2))
  			this.onRatingPress(info)
  		})
    }

	onFavoritePress = () => {
  		const { info }    = this.state,
  			  setFavorite = item => {
	  			return {
					...item,
					...{ favorite: undefined !== item['favorite']?!item['favorite']:true  }
				}
	  		  }
  		this.setState({ info: setFavorite(info) }, () => this.props.storeNews(this.props.news.map(item2 => info.publishedAt == item2.publishedAt?setFavorite(item2):item2)))
  	}

	onRatingPress = (item) => {
		this.props.showPopup({
			confirmText : 'Cancel',
          	content     : () => this.renderRateForm(item),
          	onConfirm   : () => this.props.closePopup(),
          	title       : 'Rate news'
      	})
    }

	onErrorImage = () => {
		this.setState({ image: require('../../assets/images/default.png') })
	}

	onScroll = (event) => {
		const CustomLayoutLinear = {
			duration: 100,
			create: { type: LayoutAnimation.Types.linear, property: LayoutAnimation.Properties.opacity },
			update: { type: LayoutAnimation.Types.linear, property: LayoutAnimation.Properties.opacity },
			delete: { type: LayoutAnimation.Types.linear, property: LayoutAnimation.Properties.opacity }
		}
		// Check if the user is scrolling up or down by confronting the new scroll position with your own one
		const currentOffset = event.nativeEvent.contentOffset.y
		const direction = (currentOffset > 0 && currentOffset > this.listViewOffset)
		? 'down'
		: 'up'
		// If the user is scrolling down (and the action-button is still visible) hide it
		const show_button = direction === 'up'
		if (show_button !== this.state.show_button) {
			LayoutAnimation.configureNext(CustomLayoutLinear)
			this.setState({ show_button })
		}
		// Update your scroll position
		this.listViewOffset = currentOffset
	}

	renderRateForm = (item) => {
		return (
			<Rating item={item} onSelectRating={this.rateNews} iconSize={70} />
		)
	}

	render() { 
		const { image, info, loading, show_button } = this.state
		if(loading) {
			return(<Loading style={styles.loadingcontainer} color={colors.primary} />)
		} else {
			return (
				<View style={styles.container}>
					<ScrollView style={styles.bg} onScroll={this.onScroll} ref={this.setScrollViewRef}>
						<View style={styles.header}>
							<Text style={styles.title}>{info.title}</Text>
							<Text style={styles.desc}>{info.description}</Text>
						</View>
						<View style={styles.body}>
							<View style={styles.imageContainer}>
								<Image source={image} style={styles.image} onError={this.onErrorImage} resizeMethod="resize" />
								<View style={styles.ratingContainer}>
									<Rating item={info} onPress={() => this.onRatingPress(info)} iconSize={30} />
								</View>
							</View>
							<View style={styles.textContainer}>
								<View style={styles.authorContainer}>
									<Image style={styles.authorImage} source={require('../../assets/images/avatar.png')} resizeMethod="resize" />
									<View style={styles.authorTextContainer}>
										<Text style={styles.authorName}>{getAuthor(info.author) || 'Anonymous'}</Text>
										<Text style={styles.dateText}>{moment(info.publishedAt).format('LLLL')}</Text>
									</View>
								</View>
								<View style={styles.line} />
								<Text style={styles.content}>{info.content}</Text>
							</View>
						</View>
				    </ScrollView>
				    <FloatingButton
						icon={info.favorite?'heart':'heart-outline'}
						onPress={() => this.onFavoritePress()}
						show={show_button}
					/>
				</View>
			)
		}
	}
}

const mapStateToProps = (state) => ({ news: state.news })

const mapDispatchToProps = dispatch => ({
	addNewsToFavorites : data => dispatch(addNewsToFavorites(data)),
	closePopup         : ()   => dispatch(closePopup()),
	loadingPopup       : data => dispatch(loadingPopup(data)),
	showPopup          : data => dispatch(showPopup(data)),
	storeNews          : data => dispatch(storeNews(data))
})

export default connect(mapStateToProps, mapDispatchToProps)(Show)

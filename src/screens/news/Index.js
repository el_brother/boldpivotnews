import React from 'react'
import { connect } from 'react-redux'
import { Animated, FlatList, ScrollView, Text, TouchableOpacity, View } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome5'
//Components
import Card from '../../components/Card'
import Loading from '../../components/Loading'
import Rating from '../../components/Rating'
import SearchInput from '../../components/SearchInput'
//Services
import Api from '../../api/index'
import { colors } from '../../services/Constants'
import { filterNews, isEqual } from '../../services/Utils'
//Redux
import { addNewsToFavorites, storeNews } from '../../actions/news'
import { closePopup, loadingPopup, showPopup } from '../../actions/popup'
//Libs
import _ from 'lodash'
//Styles
import styles from '../styles/ListStyle'
import error_styles from '../styles/ErrorStyle'

class News extends React.Component {
  	constructor(props) {
	    super(props)
	    //Init state
	    this.state = {
	    	loading    : false,
	    	news       : null,
	    	refreshing : false,
	    	search     : {
    			margintop : new Animated.Value(-60),
    			show      : false
    		},
	    	tosearch   : ''
	    }
	    //Local variables
        this.api  = Api.create()
        this.news = []
  	}

  	componentDidMount() {
  		this.getNews()
  	}

  	static getDerivedStateFromProps(props, state) {
  		const { news } = props
  		if(!isEqual(news,state.news)) {
  			const { tosearch } = state
			return { news: '' != tosearch?filterNews(news, tosearch):news }
  		} else {
  			return null
  		}
  	}

  	getNews = (refresh) => {
  		this.setState(refresh?{ refreshing: true }:{ loading: true }, async () => { 
  			let state    = { loading: false, refreshing: false },
  				news     = Object.assign([], this.props.news),
  				index    = -1
			try {
				const response = await this.api.news.get(),
					  { data } = response
				if(200 != response.status) {
					throw data
				}
				let news2 = data.articles.map(item => {
					index = news.findIndex(item2 => item2.publishedAt == item.publishedAt)
					if(0 <= index) {
						item = Object.assign(item, news[index])
						news.splice(index, 1)
						return item
					} else {
						return item
					}
				}).concat(news)
				state.news = _.orderBy(news2, 'publishedAt', 'desc')
				this.news  = state.news
				this.props.storeNews(state.news)
			} catch(err) {
				console.log(err)
			}
			this.setState(state)
		})
  	}

  	goToDetails = (item) => {
  		this.props.navigation.navigate('NewsShow', { id: item.publishedAt })
  	}

  	onFavoritePress = (item) => {
  		let { news } = this.state

  		news = news.map(item2 => {
  			if(item.publishedAt == item2.publishedAt) {
  				return {
  					...item2,
  					...{ favorite: undefined !== item2['favorite']?!item2['favorite']:true }
  				}
  			} else {
  				return item2
  			}
  		})
  		this.setState({ news }, () => this.props.storeNews(news))
  	}

  	onRatingPress = (item) => {
		this.props.showPopup({
			confirmText : 'Cancel',
          	content     : () => this.renderRateForm(item),
          	onConfirm   : () => this.props.closePopup(),
          	title       : 'Rate news'
      	})
    }

    rateNews = (item, rate) => {
    	let { news } = this.state

  		news = news.map(item2 => {
  			if(item.publishedAt == item2.publishedAt) {
  				return {
  					...item2,
  					...{ rating: rate }
  				}
  			} else {
  				return item2
  			}
  		})
  		this.setState({ news }, () => {
  			this.props.storeNews(news)
  			this.onRatingPress(news.find(item2 => item.publishedAt == item2.publishedAt))
  		})
    }

    toogleSearch = () => {
  		const { search } = this.state
  		this.setState({ search: { ...search, ...{ show: !search.show } } }, () => Animated.spring(search.margintop, { toValue: search.show?-60:0, useNativeDriver: false }).start())
  	}

  	search = (text) => {
  		const tosearch = text.toLowerCase()
		this.setState({ tosearch, news: '' == tosearch?this.news:filterNews(this.news, tosearch) })
  	}

    renderRateForm = (item) => {
		return (
			<Rating item={item} onSelectRating={this.rateNews} iconSize={70} />
		)
	}

  	renderEmpty = () => {
		return (
			<View style={styles.empty}>
				<Text style={styles.emptyTxt}>There are not news for the moment</Text>
			</View>
		)
	}

  	renderError = (msg) => {
		return (
			<View style={[styles.container, error_styles.error]}>
				<Text style={error_styles.errorText}>{msg}</Text>
				<TouchableOpacity style={error_styles.errorButton} onPress={() => this.getNews()}>
					<Text style={error_styles.errorButtonText}>Try again</Text>
				</TouchableOpacity>
			</View>
		)
	}

	render() { 
		const { loading, news, refreshing, search } = this.state
		if(loading) {
			return(<Loading />)
		} else if(null == news) {
				return this.renderError('Error loading the news.')
		} else {
			return (
				<View style={styles.container}>
			        <Animated.View style={[styles.tools, { marginTop: search.margintop }]}>
			        	<View style={styles.inputContainer}>
							<SearchInput onChangeText={this.search} label="Search" />
		          		</View>
			        	<TouchableOpacity onPress={() => this.toogleSearch()} style={styles.toolsButton}>
			        		<Icon name={search.show?'chevron-up':'chevron-down'} style={styles.toolsButtonIcon} />
		        		</TouchableOpacity>
			        </Animated.View>
			        <View style={styles.subContainer}>
						<FlatList
							data={news}
							extraData={this.state}
							keyExtractor={item => item.publishedAt}
							ListEmptyComponent={this.renderEmpty}
							onRefresh={() => this.getNews(true)}
							refreshing={refreshing}
							renderItem={({ item, index }) => <Card {...item} index={index} onPress={() => this.goToDetails(item)} onFavoritePress={() => this.onFavoritePress(item)} onRatingPress={() => this.onRatingPress(item)}  />}
						/>
					</View>
				</View>
			)
		}
	}
}

const mapStateToProps = (state) => ({ news: state.news })

const mapDispatchToProps = dispatch => ({
	addNewsToFavorites : data => dispatch(addNewsToFavorites(data)),
	closePopup         : ()   => dispatch(closePopup()),
	loadingPopup       : data => dispatch(loadingPopup(data)),
	showPopup          : data => dispatch(showPopup(data)),
	storeNews          : data => dispatch(storeNews(data))
})

export default connect(mapStateToProps, mapDispatchToProps)(News)

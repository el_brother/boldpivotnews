import { StyleSheet } from 'react-native'
import { colors, fonts } from '../../services/Constants'

export default StyleSheet.create({
	error: {
		alignItems: 'center',
		backgroundColor: colors.white,
		height: '100%',
		justifyContent: 'center',
		width: '100%'
	},
	errorButton: {
		alignItems: 'center',
		paddingVertical: 10
	},
	errorButtonText: {
		backgroundColor: 'transparent',
		color: colors.primary,
		fontFamily: fonts.bold,
		fontSize: 16
	},
	errorText: {
		backgroundColor: 'transparent',
		color: colors.gray,
		fontFamily: fonts.regular,
		fontSize: 16,
		textAlign: 'center'
	}
})
import { StyleSheet } from 'react-native'
import { colors, fonts } from '../../services/Constants'

export default StyleSheet.create({
	authorContainer: {
		alignItems: 'center',
		flexDirection: 'row',
		marginTop: 10,
		width: '100%'
	},
	authorImage: {
		borderColor: colors.primary,
		borderRadius: 25,
        borderWidth: 4,
		height: 50,
		width: 50
	},
	authorName: {
		backgroundColor: 'transparent',
		color: colors.primary,
		fontFamily: fonts.bold,
		fontSize: 12
	},
	authorTextContainer: {
		marginLeft: 10
	},
	bg: {
		backgroundColor: colors.white,
		height: '100%'
	},
	body: {
		backgroundColor: colors.secondary,
		width: '100%'
	},
	container: {
		alignItems: 'center',
        backgroundColor: colors.white,
		justifyContent: 'flex-start',
	},
	content: {
		backgroundColor: 'transparent',
		color: colors.black,
		fontFamily: fonts.regular,
		fontSize: 14,
		marginTop: 10
	},
	dateContainer: {
		alignItems: 'center',
		flexDirection: 'row',
		marginTop: 10,
		width: '100%'
	},
	dateIcon: {
		backgroundColor: 'transparent',
		color: colors.gray,
		fontFamily: fonts.bold,
		fontSize: 16,
		marginRight: 5
	},
	dateText: {
		backgroundColor: 'transparent',
		color: colors.gray,
		fontFamily: fonts.regular,
		fontSize: 12
	},
	desc: {
		backgroundColor: 'transparent',
		color: colors.white,
		fontFamily: fonts.regular,
		fontSize: 14
	},
	header: {
		alignItems: 'center',
		backgroundColor: colors.primary,
		justifyContent: 'center',
		paddingHorizontal: 10,
		paddingBottom: 30,
		width: '100%'
	},
	imageContainer: {
		height: 200,
		width: '100%'
	},
	image: {
		height: '100%',
		width: '100%'
	},
	line: {
		backgroundColor: 'transparent',
		borderColor: colors.light_gray,
		borderBottomWidth: 2,
		marginTop: 20,
		width: 75
	},
	ratingContainer: {
		alignItems: 'flex-end',
		backgroundColor: 'rgba(47, 53, 67, 0.75)',
		justifyContent: 'flex-end',
		marginTop: -40,
		width: '100%'
	},
	textContainer: {
		backgroundColor: colors.white,
		height: '100%',
		padding: 10
	},
	title: {
		backgroundColor: 'transparent',
		color: colors.white,
		fontFamily: fonts.black,
		fontSize: 20,
		marginBottom: 5
	}
})
import { StyleSheet } from 'react-native'
import { colors, fonts } from '../../services/Constants'

export default StyleSheet.create({
	container: {
		alignItems: 'center',
        backgroundColor: colors.container,
		height: '100%',
		justifyContent: 'flex-start',
	    width: '100%'
	},
	empty: {
		marginTop: 20,
		width: '100%'
	},
	emptyTxt: {
		backgroundColor: 'transparent',
		color: colors.white,
		fontFamily: fonts.regular,
		fontSize: 14,
		textAlign: 'center'
	},
	inputContainer: {
		marginBottom: 10,
		width: '100%'
	},
	tools: {
		alignItems: 'center',
		backgroundColor: colors.primary,
		padding: 10,
		width: '100%',
		zIndex: 0
	},
	toolsButton: {
		alignItems: 'center',
		backgroundColor: 'transparent',
		width: '100%'
	},
	toolsButtonIcon: {
		color: colors.white,
		fontSize: 20
	},
	subContainer: {
		alignItems: 'center',
        backgroundColor: colors.container,
		justifyContent: 'flex-start',
		padding: 10,
		paddingBottom: 55,
	    width: '100%'
	}
})
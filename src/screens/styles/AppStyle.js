import { StyleSheet } from 'react-native'

export default StyleSheet.create({
	app: {
		flex: 1,
		height: '100%',
		justifyContent: 'center',
		width: ' 100%'
	}
})

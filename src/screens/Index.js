import React, { Component } from 'react'
import { View }from 'react-native'
import { connect } from 'react-redux'
//Redux
import { closePopup } from '../actions/popup'
//Services
import { Unlogged } from '../config/Navigation'
import { colors } from '../services/Constants'
//Components
import Loading from '../components/Loading'
import Popup from '../components/Popup'
//Styles
import styles from './styles/AppStyle'

class Index extends Component {

	constructor(props) {
	    super(props)
	    //Init state
	    this.state = { popup: this.props.popup }
	}

	static getDerivedStateFromProps(props, state) {
	    if (props.popup !== state.popup) {
	      return { popup: props.popup }
	    }
	    return null
  	}

	render() {
		const { popup } = this.state
		return (
			<View style={styles.app}>
				<Unlogged />
				<Popup
	          		cancelText={popup.cancelText}
		          	confirmText={popup.confirmText}
		          	content={'loading' == popup.status?(<Loading style={styles.loadingcontainer} color={colors.primary} />):('function' == typeof popup.content?popup.content():popup.content)}
		          	onClose={popup.onClose?popup.onClose:this.props.closePopup}
		          	onConfirm={popup.onConfirm}
		          	show={popup.show || false}
		          	title={popup.title || ''}
	          	/>
 	      	</View>
		)
	}
}

const mapStateToProps = (state) => ({ popup: state.popup })

export default connect(mapStateToProps)(Index)

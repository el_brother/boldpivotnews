import { newsReducer } from './news'
import { popupReducer } from './popup'

export default {
	news  : newsReducer,
	popup : popupReducer
}
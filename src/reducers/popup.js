export const popupReducer = (state = {}, action) => {
	const { payload } = action
	switch (action.type) {
		case 'CLOSE':
   		if('function' == typeof payload) {
   			payload()
   		}
  		return { ...state, ...{ show: false } }
    case 'LOADING':
      return { status: 'loading', title: payload, show: true }
   	case 'SHOW':
  		return { ...payload, ...{ show: true } }
  	default:
  		return state
	}
}

import { REHYDRATE } from 'redux-persist'
import Immutable from 'seamless-immutable'

export const newsReducer = (state = [], action) => {
  const { payload } = action
	switch (action.type) { 
  	case REHYDRATE:
    	return payload && payload.news?payload.news:state
   	case 'STORE_NEWS':
  		return payload
  	case 'ADD_NEWS_TO_FAVORITE':
		let news = Immutable.asMutable(state)
		const index = news.findIndex(item => item.publishedAt == payload)
		let obj = news[index]
		obj['favorite'] = undefined != obj['favorite']?!obj['favorite']:true
		news[index] = obj
  		return state
  	default:
  		return state
  	}
}

import axios from 'axios'
import { API_URL, API_PATH, CACHE_CONTROL, CONTENT_TYPE, TIMEOUT } from './constants'

const create = () => {
	let headers = {
		'Accept'        : CONTENT_TYPE,
		'Cache-Control' : CACHE_CONTROL,
		'Content-Type'  : CONTENT_TYPE
	}

  	let instance = axios.create({
		baseURL        : API_URL,
		headers,
		timeout        : TIMEOUT,
		validateStatus : () => true
	})
  
  	return {
    	news: {
			get: () => instance.get(`${API_PATH}top-headlines?country=us&apiKey=8a794654c5f54ca99f72ead68da28d58`)
  		}
	}
}

export default { create }

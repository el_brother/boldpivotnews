export const API_URL       = 'https://newsapi.org/'
export const API_PATH      = 'v2/'
export const CACHE_CONTROL = 'no-cache'
export const CONTENT_TYPE  = 'application/json'
export const TIMEOUT       = 15000

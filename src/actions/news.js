export const storeNews = data => ({ type: 'STORE_NEWS', payload : data })

export const addNewsToFavorites = data => ({ type: 'ADD_NEWS_TO_FAVORITE', payload : data })

export const closePopup   = data => ({ type: 'CLOSE', payload : data })
export const loadingPopup = data => ({ type: 'LOADING', payload : data })
export const showPopup    = data => ({ type: 'SHOW', payload : data })
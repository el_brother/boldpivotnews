import React from 'react'
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator, CardStyleInterpolators } from '@react-navigation/stack'
//Screens
import News from '../screens/news/Index'
import NewsShow from '../screens/news/Show'
import Header from '../components/Header'

const Stack = createStackNavigator()

export const Unlogged = () => {
	return (
		<NavigationContainer>
			<Stack.Navigator
				initialRouteName="Home"
				screenOptions={{
					cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
					header : props => <Header {...props} />
				}}
			>
				<Stack.Screen
					name="Home"
					component={News}
					options={{ title: 'Top Headlines overview' }}
				/>
				<Stack.Screen
					name="NewsShow"
					component={NewsShow}
					options={{ title: 'Top headline details' }}
				/>
			</Stack.Navigator>
		</NavigationContainer>
	)
}

import { createStore, compose, applyMiddleware } from 'redux'
import { persistStore, persistCombineReducers } from 'redux-persist'
import ReduxPersist from '../config/ReduxPersist'
import rootReducers from '../reducers/index'

const enhancers = [applyMiddleware(...[])]

export const store     = createStore(persistCombineReducers(ReduxPersist, rootReducers), undefined, compose(...enhancers))
export const persistor = persistStore(store, { enhancers })


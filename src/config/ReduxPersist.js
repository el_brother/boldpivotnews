import Transform from './Transforms'
import { AsyncStorage } from 'react-native'
import autoMergeLevel2 from 'redux-persist/lib/stateReconciler/autoMergeLevel2'

export default {
    key             : 'root',
    stateReconciler : autoMergeLevel2,
    storage         : AsyncStorage,
    transforms      : [Transform],
    whitelist       : ['news']
}


export const colors  = {
    black      : '#1e1e1e',
    container  : '#586575',
    gray       : '#828b92',
    green      : '#1cc09e',
    light_gray : '#c4c4c4',
    primary    : '#e01e5a',
    secondary  : '#2f3543',
    white      : '#ffffff',
    yellow     : '#f4cb42'
}
export const fonts = {
	black   : 'Roboto-Black',
	bold    : 'Roboto-Bold',
	regular : 'Roboto-Regular'
}
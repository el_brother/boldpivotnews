import React from 'react'
import { ActivityIndicator, StatusBar, View } from 'react-native'
import SplashScreen from 'react-native-smart-splash-screen'
import { Provider } from 'react-redux'
import { PersistGate } from 'redux-persist/integration/react'
//Components
import Loading from './src/components/Loading'
//Config
import { persistor, store } from './src/config/Store'
import Index from './src/screens/Index'

console.disableYellowBox = true

export default class App extends React.Component {
  
  componentDidMount() {
    SplashScreen.close({
      animationType : SplashScreen.animationType.scale,
      duration      : 850,
      delay         : 500
    })
  }

  render() {
    return (
      <Provider store={store}>
        <PersistGate loading={<Loading />} persistor={persistor}>
          <StatusBar backgroundColor="rgba(224, 30, 90, 0.75)" animated hidden />
          <Index />
        </PersistGate>
      </Provider>
    )
  }
}